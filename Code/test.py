import pandas as pd
import numpy as np
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import nltk
from sklearn import svm
from pycontractions import Contractions
from sklearn.model_selection import GridSearchCV
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import TweetTokenizer
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import classification_report, confusion_matrix 
import emoji
import re
import os

#had to set to java 8 to satisfy problems with language-check. Language-check is pycontractions dependency
# os.setenv("JAVA_8_HOME")
os.environ['JAVA_HOME'] = os.getenv("JAVA_8_HOME")
# print(os.environ)
pd.options.mode.chained_assignment = None
nltk.download('stopwords')
nltk.download('wordnet')

cont = Contractions('GoogleNews-vectors-negative300.bin')
cont.load_models()

def clean_file(filename):
    #check the null etc kinds of data on one item. Likely that all items will have a similar structure. Basically try and figure
    #out how empty and null items are listed
    filepath = './data/' + filename
    main = pd.read_csv(filepath)
    # print(main.head())
    #head shows there are plenty of NaN values

    #create a copt for modifications. This is just in case we want to reset values anywhere and don't end up messing with the data.
    main_copy = main.copy()

    #check for na values
    # print(main_copy.isna().sum())

    #some elements from above have all columns that are NaN. So those columns are useless and we should remove them
    #print shape before and after to see if na elements are removed afterwards
    # print(main_copy.shape)
    #columns
    main_copy.dropna(axis=1, how='all', inplace=True)
    #rows
    main_copy.dropna(axis=0, how='all', inplace=True)
    # print(main_copy.shape)

    #check all columns
    # print(main_copy.columns)
    #remove all unnecessary columns
    main_copy.drop(columns=['hashtags', 'id' ,'urls', 'language', 'mentions', 'place', 'user_id', 'created_at', 'time', 'conversation_id','reply_to', 'thumbnail', 'video', 'quote_url', 'retweet', 'link', 'cashtags', 'photos', 'timezone', 'name'], inplace=True)
    # print(main_copy.shape)

    #remove duplicates
    main_copy.drop_duplicates(subset='tweet')

    #too much data...limit to 1000 tweets per day
    return main_copy.groupby('date').head(10)


all_files = os.listdir('./data')
data_frames = []

for filename in all_files:
    #below line to not consider the ipynb checkpoints file
    if not filename.startswith('.'):
        data_frames.append(clean_file(filename))
    
#combine
df = pd.concat(data_frames)

analyzer = SentimentIntensityAnalyzer()
df['score'] = df['tweet'].apply(lambda x: analyzer.polarity_scores(x)['compound'])

conditions = [df.score >= .05, df.score <= -.05]

choices = [
    'positive',
    'negative'
]

df['sentiment'] = np.select(conditions, choices, 'neutral')

#made from wikipedia list
emots = {
    "happy" : ":‑) :) :-] :] :-3 :3 :-> :> 8-) 8) :-} :} :o) :c) :^) =] =)",
    "laughing" : ":‑D :D 8‑D 8D x‑D xD X‑D XD =D =3 B^D"    ,
        "sad" : ":‑( :( :‑c :c :‑< :< :‑[ :[ :-|| >:[ :{ :@ :( ;(",
    "crying" : ":'‑( :'(",
    "happiness" : ":'‑) :')",
"sad" : "D‑': D:< D: D8 D; D= DX",
"surprise" : ":‑O :O :‑o :o :-0 8‑0 >:O",
"kiss" : ":-* :* :×",
"wink" : ";‑) ;) *-) *) ;‑] ;] ;^) :‑, ;D",
"playful" : ":‑P :P X‑P XP x‑p xp :‑p :p :‑Þ :Þ :‑þ :þ :‑b :b d: =p >:P",
"annoyed" : ":‑/ :/ :‑. >:\ >:/ :\ =/ =\ :L =L :S",
"indecision" : ":‑| :|",
"sick" : ":‑###.. :###..",
"disaproval" : "',:-| ',:-l"
        }

final_emoji_dict = {}

#since we want emojis as keys to make the fetch faster, we will manipulate the emots dictionary
for key, value in emots.items():
    emots_array = value.split(' ')
    for value in emots_array:
        final_emoji_dict[value] = key


#iterate over each tweet and clean it
def nlp_engineering_tweet(tweet, stopwords):
    temp = str(tweet).lower()
    temp = re.sub(r"(http|@)\S+", "", temp)
    
    tweet_split = temp.split(' ')
    for index, word in enumerate(tweet_split):
        if word in final_emoji_dict:
            tweet_split[index] = final_emoji_dict[word]
            
            
    temp = ' '.join(tweet_split)
    
    #to make emoji into proper words so can't use a tokenizer as that will convert them to utc
    temp = emoji.demojize(temp)
    temp = temp.replace(':', ' ')
    temp = list(cont.expand_texts([temp]))[0]
    
    tokenizer = TweetTokenizer()
    result = tokenizer.tokenize(temp)
    
    #see how looool changes from lemmatizer
    lemmatizer = WordNetLemmatizer()
    words = [lemmatizer.lemmatize(word) for word in result if word not in stopwords]
    
    return ' '.join(words)


#stop words
stop_words = stopwords.words('english')
stop_words += ['covid', '#covid',
               'covid19', '#covid19',
               'corona', '#corona',
               'coronavirus', '#coronavirus',
                'vaccine', '#vaccine']

#initially had a for loop. Changed to apply as it operates on entire column and makes it faster.
df['clean'] = df['tweet'].apply(lambda x: nlp_engineering_tweet(x, stop_words))
#writing to csv just to view it since loading in jupyter can be slow
# First we remove generic stop words in the english language from the nltk corpus
# df.to_csv('cleaned.csv')

#use n_gram_range????
tf_idf = TfidfVectorizer(min_df=200)
data_tf_idf = tf_idf.fit_transform(df.clean)

X_final = pd.DataFrame(data_tf_idf.toarray(), columns=tf_idf.get_feature_names())
y = df['sentiment']

X_train_vec, X_test_vec, y_train, y_test = train_test_split(X_final, y, test_size=0.3, stratify=y, random_state=1)
param_grid = {'C': [0.1, 1, 10, 100, 1000], 'gamma': [1, 0.1, 0.01, 0.001, 0.0001], 'kernel': ['rbf']}

grid = GridSearchCV(svm.SVC(), param_grid, refit = True, verbose = 3) 
grid.fit(X_train_vec, y_train)

y_preds = grid.predict(X_test_vec)

print(classification_report(y_test, y_preds))